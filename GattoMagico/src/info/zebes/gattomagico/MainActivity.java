package info.zebes.gattomagico;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ImageButton bottoneMagico = (ImageButton)findViewById(R.id.bottoneMagico);
		bottoneMagico.setOnClickListener(new OnClickListener() {
		      public void onClick(View v)
		      {
		    	  Toast.makeText(getApplicationContext(), "Magic Cat, scelgo te!", Toast.LENGTH_LONG).show();
		      }
		});
		
		Button bottoneK = (Button)findViewById(R.id.bottoneK);
		bottoneK.setOnClickListener(new OnClickListener() {
		      public void onClick(View v)
		      {
		    	  Intent j = new Intent(Intent.ACTION_VIEW);
		    	  j.setData(Uri.parse("http://abaluth.forumfree.it/"));
		    	  startActivity(j);
		      }
		});

		ToggleButton harlemShake = (ToggleButton)findViewById(R.id.bottoneCoso);
		harlemShake.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				FestinBueo magic = new FestinBueo();
				if(isChecked) magic.start();
				else magic.stop();
			}
		});
		
		RatingBar posterdati = (RatingBar)findViewById(R.id.ratingBar1);
		posterdati.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {

			@Override
			public void onRatingChanged(RatingBar arg0, float arg1, boolean arg2) {
				if(arg1 == 0.0)
					Toast.makeText(getApplicationContext(), "Gatto non magico pi� Jjjjj", Toast.LENGTH_LONG).show();
				else
					if(arg1 < 4.9)
						Toast.makeText(getApplicationContext(), "Punteggio magico: "+arg1, Toast.LENGTH_LONG).show();
					else
						Toast.makeText(getApplicationContext(), "M5S Jjjjjjjjjj", Toast.LENGTH_LONG).show();
			}
			
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	class FestinBueo implements Runnable {
		int[] magicColors = {Color.BLACK, Color.BLUE, Color.CYAN, Color.DKGRAY, Color.GRAY, Color.GREEN, Color.LTGRAY, Color.MAGENTA, Color.RED, Color.TRANSPARENT, Color.WHITE, Color.YELLOW};
		private volatile Thread blinker;
		
		public void start() {
			blinker = new Thread(this);
			blinker.start();
		}
		
		public synchronized void stop() {
	        blinker = null;
	    }
		
		public void run() {
			Thread thisThread = Thread.currentThread();
			while (blinker == thisThread)
				for(final int j: magicColors) {
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						
					}
					runOnUiThread(new Runnable() {
						public void run() {
							((RelativeLayout)findViewById(R.id.terrorista)).setBackgroundColor(j);
						}
					});
				}
		}

	}

}
